

// 180° == πradians == 0x80000000 32 bit semicircles (decimal form)
const PI_RADIANS = 2147483648
// Semicircles per meter at the equator
const SC_PER_METER = 107.173;

// helper functions
function degreesToRadians(degrees) {
  return degrees * Math.PI / 180;
}

function semicirclesToRadians(semicirlces) {
return (semicirlces * Math.PI) / PI_RADIANS;
}

function semicirclesToDegrees(semicirlces) {
return (semicirlces * (180 / PI_RADIANS));
}

// Sample GPS coordinates, in semicircles
var coordinates = [
{lat:476914587,lon:-1254520202},
{lat:476914647,lon:-1254520081},
{lat:476914726,lon:-1254519979},
{lat:476914862,lon:-1254519795},
{lat:476915068,lon:-1254519544},
{lat:476915367,lon:-1254519275},
{lat:476915725,lon:-1254518926},
{lat:476916045,lon:-1254518539},
{lat:476916311,lon:-1254518154},
{lat:476916484,lon:-1254517787},
];

// taking all latitudes and putting them into a array
let latitudes = coordinates.map(a => a.lat);
// taking all longitudes and putting them into a array
let longitudes = coordinates.map(b => b.lon);

// calculates distance between two coordinates
// takes in four arguments, latitude1, longitude1, latitude2, longitude2 (all in degrees)
function haversineDistance(lat1, lon1, lat2, lon2) {
      // earths r in m
      var earthR = 6371*1000;
      // Lat and Long for haversine
      var hLat = degreesToRadians(lat2-lat1);
      var hLon = degreesToRadians(lon2-lon1);

      //conversion to radians
      lat1 = degreesToRadians(lat1);
      lat2 = degreesToRadians(lat2);

      // formula implementation
      // https://en.wikipedia.org/wiki/Haversine_formula
      var a = Math.sin(hLat/2) * Math.sin(hLat/2) + Math.sin(hLon/2) * Math.sin(hLon/2) * Math.cos(lat1) * Math.cos(lat2);
      // arcsin implementation
      var hs = 2 * Math.asin(Math.sqrt(a))
      return earthR * hs;
    }

// call this function on console to output answer
// loops through latitude and longitude arrays and uses haversineDistance
// adds answer + previous total and outputs it

function answer(){
      var total = 0;
    for (var i = 0; i < latitudes.length - 1; i++){
      total += haversineDistance(semicirclesToDegrees(latitudes[i]),semicirclesToDegrees(longitudes[i]),semicirclesToDegrees(latitudes[i+1]),semicirclesToDegrees(longitudes[i+1]))
      }
      console.log(total + " meters")
    }
